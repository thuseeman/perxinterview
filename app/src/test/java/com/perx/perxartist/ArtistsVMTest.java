package com.perx.perxartist;

import com.perx.perxartist.artists.ArtistsVM;
import com.perx.perxartist.data.model.Status;
import com.perx.perxartist.data.remote.ApiService;
import com.perx.perxartist.data.remote.response.ArtistsResponse;
import com.perx.perxartist.util.TestDataUtils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Thuseeman on 3/22/2017.
 */

public class ArtistsVMTest {
    private ApiService mApiService;
    private ArtistsVM mArtistsVM;
    private TestSubscriber<Status> mTestSubscriber;

    @Before
    public void setupVM() {
        mApiService = Mockito.mock(ApiService.class);
        mArtistsVM = new ArtistsVM(mApiService);
        mTestSubscriber = TestSubscriber.create();
        when(mApiService.fetchArtists()).thenReturn(createArtistsResponse());
    }

    @Test
    public void fetchArtistsSuccess(){
        mArtistsVM.fetchArtists().subscribe(mTestSubscriber);
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertCompleted();
        List<Status> statusList = mTestSubscriber.getOnNextEvents();
        assertEquals(statusList.size(), 1);
        assertEquals(statusList.get(0).success, true);
        assertEquals(statusList.get(0).message, null);
    }

    @Test
    public void fetchArtists(){
        mArtistsVM.fetchArtists().subscribe();
        assertEquals(mArtistsVM.getArtists().size(), 2);
    }

    @Test
    public void fetchNoArtists(){
        when(mApiService.fetchArtists()).thenReturn(createEmptyArtistsResponse());
        mArtistsVM.fetchArtists().subscribe();
        assertEquals(mArtistsVM.getArtists().size(), 0);
    }

    public static Observable<Response<ArtistsResponse>> createArtistsResponse(){
        ArtistsResponse artistsResponse = new ArtistsResponse();
        artistsResponse.artists = new ArrayList<>();
        artistsResponse.artists.add(TestDataUtils.createArtist(1));
        artistsResponse.artists.add(TestDataUtils.createArtist(2));
        return Observable.just(Response.success(artistsResponse));
    }

    public static Observable<Response<ArtistsResponse>> createEmptyArtistsResponse(){
        ArtistsResponse artistsResponse = new ArtistsResponse();
        artistsResponse.artists = new ArrayList<>();
        return Observable.just(Response.success(artistsResponse));
    }

}
