package com.perx.perxartist;

import com.perx.perxartist.aritst.ArtistVM;
import com.perx.perxartist.data.model.Status;
import com.perx.perxartist.data.remote.ApiService;
import com.perx.perxartist.data.remote.response.AlbumResponse;
import com.perx.perxartist.util.TestDataUtils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Thuseeman on 3/22/2017.
 */

public class ArtistVMTest {
    private ApiService mApiService;
    private ArtistVM mArtistVM;
    private TestSubscriber<Status> mTestSubscriber;

    @Before
    public void setupVM() {
        mApiService = Mockito.mock(ApiService.class);
        mArtistVM = new ArtistVM(mApiService, TestDataUtils.createArtist(1));
        mTestSubscriber = TestSubscriber.create();
        when(mApiService.fetchAlbums(1)).thenReturn(createAlbumResponse());
    }

    @Test
    public void fetchAlbumsSuccess(){
        mArtistVM.fetchAlbums().subscribe(mTestSubscriber);
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertCompleted();
        List<Status> statusList = mTestSubscriber.getOnNextEvents();
        assertEquals(statusList.size(), 1);
        assertEquals(statusList.get(0).success, true);
        assertEquals(statusList.get(0).message, null);
    }

    @Test
    public void fetchAlbums(){
        mArtistVM.fetchAlbums().subscribe();
        assertEquals(mArtistVM.getAlbums().size(), 2);
    }

    @Test
    public void fetchNoAlbums(){
        when(mApiService.fetchAlbums(1)).thenReturn(createEmptyAlbumResponse());
        mArtistVM.fetchAlbums().subscribe();
        assertEquals(mArtistVM.getAlbums().size(), 0);
    }

    private Observable<Response<AlbumResponse>> createAlbumResponse(){
        AlbumResponse albumResponse = new AlbumResponse();
        albumResponse.albums = new ArrayList<>();
        albumResponse.albums.add(TestDataUtils.createAlbum(1));
        albumResponse.albums.add(TestDataUtils.createAlbum(2));
        return Observable.just(Response.success(albumResponse));
    }

    private static Observable<Response<AlbumResponse>> createEmptyAlbumResponse(){
        AlbumResponse albumResponse = new AlbumResponse();
        albumResponse.albums = new ArrayList<>();
        return Observable.just(Response.success(albumResponse));
    }
}
