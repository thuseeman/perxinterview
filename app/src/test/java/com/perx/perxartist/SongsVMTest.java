package com.perx.perxartist;

import com.perx.perxartist.data.model.Status;
import com.perx.perxartist.data.remote.ApiService;
import com.perx.perxartist.data.remote.response.SongsResponse;
import com.perx.perxartist.songs.SongsVM;
import com.perx.perxartist.util.TestDataUtils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Thuseeman on 3/22/2017.
 */

public class SongsVMTest {
    private ApiService mApiService;
    private SongsVM mSongVM;
    private TestSubscriber<Status> mTestSubscriber;

    @Before
    public void setup(){
        mApiService = Mockito.mock(ApiService.class);
        mSongVM = new SongsVM(mApiService, TestDataUtils.createArtist(1));
        mTestSubscriber = TestSubscriber.create();
        when(mApiService.fetchSongs(1,1)).thenReturn(createSongs2Response());
        when(mApiService.fetchSongs(1,2)).thenReturn(createSongsPage2Response());
    }

    @Test
    public void fetchSongsSuccess(){
        mSongVM.fetchSongs().subscribe(mTestSubscriber);
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertCompleted();
        List<Status> statusList = mTestSubscriber.getOnNextEvents();
        assertEquals(statusList.size(), 1);
        assertEquals(statusList.get(0).success, true);
        assertEquals(statusList.get(0).message, null);
    }

    @Test
    public void fetch2Songs(){
        mSongVM.fetchSongs().subscribe();
        assertEquals(mSongVM.getSongs().size(), 2);
        assertEquals(mSongVM.getPageNo(), 1);
        assertEquals(mSongVM.isLastPage(), true);
    }

    @Test
    public void fetch10Songs(){
        when(mApiService.fetchSongs(1,1)).thenReturn(createSongs10Response());
        mSongVM.fetchSongs().subscribe();
        assertEquals(mSongVM.getSongs().size(), 10);
        assertEquals(mSongVM.getPageNo(), 2);
        assertEquals(mSongVM.isLastPage(), false);

        //next page
        mSongVM.fetchSongs().subscribe();
        assertEquals(mSongVM.getSongs().size(), 12);
        assertEquals(mSongVM.getPageNo(), 2);
        assertEquals(mSongVM.isLastPage(), true);
    }

    @Test
    public void fetchNoSongs(){
        when(mApiService.fetchSongs(1, 1)).thenReturn(createEmptySongsResponse());
        mSongVM.fetchSongs().subscribe();
        assertEquals(mSongVM.getSongs().size(), 0);
        assertEquals(mSongVM.getPageNo(), 1);
        assertEquals(mSongVM.isLastPage(), true);
    }

    private Observable<Response<SongsResponse>> createSongs2Response(){
        SongsResponse songsResponse = new SongsResponse();
        songsResponse.songs = new ArrayList<>();
        songsResponse.songs.add(TestDataUtils.createSong(1));
        songsResponse.songs.add(TestDataUtils.createSong(2));
        return Observable.just(Response.success(songsResponse));
    }

    private Observable<Response<SongsResponse>> createSongs10Response(){
        SongsResponse songsResponse = new SongsResponse();
        songsResponse.songs = new ArrayList<>();
        songsResponse.songs.add(TestDataUtils.createSong(1));
        songsResponse.songs.add(TestDataUtils.createSong(2));
        songsResponse.songs.add(TestDataUtils.createSong(3));
        songsResponse.songs.add(TestDataUtils.createSong(4));
        songsResponse.songs.add(TestDataUtils.createSong(5));
        songsResponse.songs.add(TestDataUtils.createSong(6));
        songsResponse.songs.add(TestDataUtils.createSong(7));
        songsResponse.songs.add(TestDataUtils.createSong(8));
        songsResponse.songs.add(TestDataUtils.createSong(9));
        songsResponse.songs.add(TestDataUtils.createSong(10));
        return Observable.just(Response.success(songsResponse));
    }

    private Observable<Response<SongsResponse>> createEmptySongsResponse(){
        SongsResponse songsResponse = new SongsResponse();
        songsResponse.songs = new ArrayList<>();
        return Observable.just(Response.success(songsResponse));
    }

    private Observable<Response<SongsResponse>> createSongsPage2Response(){
        SongsResponse songsResponse = new SongsResponse();
        songsResponse.songs = new ArrayList<>();
        songsResponse.songs.add(TestDataUtils.createSong(3));
        songsResponse.songs.add(TestDataUtils.createSong(4));
        return Observable.just(Response.success(songsResponse));
    }
}
