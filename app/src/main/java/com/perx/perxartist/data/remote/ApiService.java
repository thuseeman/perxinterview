package com.perx.perxartist.data.remote;


import com.perx.perxartist.data.remote.response.AlbumResponse;
import com.perx.perxartist.data.remote.response.ArtistsResponse;
import com.perx.perxartist.data.remote.response.SongsResponse;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public interface ApiService {

    @GET("artists")
    Observable<Response<ArtistsResponse>> fetchArtists();

    @GET("albums")
    Observable<Response<AlbumResponse>> fetchAlbums(@Query("artist_id") int id);

    @GET("songs")
    Observable<Response<SongsResponse>> fetchSongs(@Query("artist_ids") int id, @Query("page") int pageNo);
}
