package com.perx.perxartist.data.model;

/**
 * Created by Thuseeman on 3/22/2017.
 */

public class Status {
    public boolean success;
    public String message;

    public Status(){}

    public Status(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
}
