package com.perx.perxartist.data.remote.response;


import com.perx.perxartist.data.model.Song;

import java.util.List;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class SongsResponse {
    public List<Song> songs;
}
