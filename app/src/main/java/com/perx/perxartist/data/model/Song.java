package com.perx.perxartist.data.model;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class Song {
    public int id;
    public String title;
    public String href;

    public Song(){}

    public Song(int id, String title, String href) {
        this.id = id;
        this.title = title;
        this.href = href;
    }
}
