package com.perx.perxartist.data.model;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class Album {
    public int id;
    public String title;
    public int year;

    public Album(){}

    public Album(int id, String title, int year) {
        this.id = id;
        this.title = title;
        this.year = year;
    }
}
