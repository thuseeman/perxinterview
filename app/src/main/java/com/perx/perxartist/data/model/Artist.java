package com.perx.perxartist.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class Artist implements Parcelable {
    public int id;
    public String name;
    public String website;
    public String href;

    public Artist(int id, String name, String website, String href) {
        this.id = id;
        this.name = name;
        this.website = website;
        this.href = href;
    }

    protected Artist(Parcel in) {
        id = in.readInt();
        name = in.readString();
        website = in.readString();
        href = in.readString();
    }

    public static final Creator<Artist> CREATOR = new Creator<Artist>() {
        @Override
        public Artist createFromParcel(Parcel in) {
            return new Artist(in);
        }

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(website);
        parcel.writeString(href);
    }
}
