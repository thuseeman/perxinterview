package com.perx.perxartist.artists;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.perx.perxartist.R;
import com.perx.perxartist.aritst.ArtistActivity;
import com.perx.perxartist.data.model.Artist;
import com.perx.perxartist.databinding.ArtistItemBinding;
import com.perx.perxartist.util.Utils;

import java.util.List;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class ArtistsAdapter extends RecyclerView.Adapter<ArtistsAdapter.ViewHolder> {
    private List<Artist> mList;

    public ArtistsAdapter(List<Artist> artists){
        mList = artists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ArtistItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.artist_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Artist artist = mList.get(position);

        holder.binding.name.setText(artist.name);

        holder.binding.website.setOnClickListener(view -> {
            Utils.openBrowser(view.getContext(), artist.website);
        });

        holder.itemView.setOnClickListener(view -> {
            Context context = view.getContext();
            Intent intent = new Intent(context, ArtistActivity.class);
            intent.putExtra("artist", artist);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ArtistItemBinding binding;
        public ViewHolder(ArtistItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
