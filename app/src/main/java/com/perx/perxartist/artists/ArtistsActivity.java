package com.perx.perxartist.artists;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;

import com.perx.perxartist.R;
import com.perx.perxartist.base.BaseActivity;
import com.perx.perxartist.data.remote.RemoteDataSource;
import com.perx.perxartist.databinding.ArtistsActivityBinding;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ArtistsActivity extends BaseActivity {

    private ArtistsActivityBinding mBinding;
    private ArtistsVM mArtistsVM;
    private CompositeSubscription mSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.artists_activity);
        mSubscription = new CompositeSubscription();
        mArtistsVM = new ArtistsVM(RemoteDataSource.create());

        mBinding.artists.setHasFixedSize(true);
        mBinding.artists.setLayoutManager(new LinearLayoutManager(this));
        mBinding.artists.setAdapter(new ArtistsAdapter(mArtistsVM.getArtists()));

        mBinding.swipeRefresh.setOnRefreshListener(() -> {
            loadArtists();
        });

        loadArtists();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSubscription.clear();
    }

    private void loadArtists() {
        mSubscription.add(
                mArtistsVM.fetchArtists()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(() -> mBinding.swipeRefresh.setRefreshing(true))
                        .doOnTerminate(() -> mBinding.swipeRefresh.setRefreshing(false))
                        .subscribe(response -> {
                            if (response.success) {
                                mBinding.artists.getAdapter().notifyDataSetChanged();
                            }else {
                                if (TextUtils.isEmpty(response.message)) {
                                    showToast(response.message);
                                }
                            }
                        }, t -> {
                            handleServerError(t);
                        })
        );
    }
}
