package com.perx.perxartist.artists;

import com.perx.perxartist.data.model.Artist;
import com.perx.perxartist.data.model.Status;
import com.perx.perxartist.data.remote.ApiService;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Thuseeman on 3/22/2017.
 */

public class ArtistsVM {
    private List<Artist> mArtists = new ArrayList<>();
    private ApiService mApiService;

    public ArtistsVM(ApiService apiService){
        mApiService = apiService;
    }

    public List<Artist> getArtists() {
        return mArtists;
    }

    public Observable<Status> fetchArtists(){
        return mApiService.fetchArtists()
                .map(response -> {
                    Status status = new Status();
                    if (response.isSuccessful()){
                        if (response.body() != null && response.body().artists != null) {
                            mArtists.clear();
                            mArtists.addAll(response.body().artists);
                            status.success = true;
                        }else {
                            status.message = "No data available";
                        }
                    }else {
                        status.message = "Please try again";
                    }
                    return status;
                });
    }
}
