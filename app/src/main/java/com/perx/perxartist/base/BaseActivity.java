package com.perx.perxartist.base;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.net.UnknownHostException;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void handleServerError(Throwable t){
        t.printStackTrace();
        if (t instanceof UnknownHostException){
            showToast("Please connect to Internet");
        }else {
            showToast("Request failed please try again");
        }
    }

}
