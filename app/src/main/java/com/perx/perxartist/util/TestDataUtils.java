package com.perx.perxartist.util;

import com.perx.perxartist.data.model.Album;
import com.perx.perxartist.data.model.Artist;
import com.perx.perxartist.data.model.Song;

/**
 * Created by Thuseeman on 3/22/2017.
 */

public class TestDataUtils {

    public static Artist createArtist(int i){
        return new Artist(i, "thuseeman "+i, "thuseeman"+i+".com", "href "+i);
    }

    public static Album createAlbum(int i){
        return new Album(i, "title "+i, 2000+i);
    }

    public static Song createSong(int i){
        return new Song(i, "test song "+i, "href "+i);
    }

}
