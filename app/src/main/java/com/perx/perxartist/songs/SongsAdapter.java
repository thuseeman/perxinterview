package com.perx.perxartist.songs;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.perx.perxartist.R;
import com.perx.perxartist.data.model.Song;
import com.perx.perxartist.databinding.SongItemBinding;

import java.util.List;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.ViewHolder> {
    private List<Song> mList;

    public SongsAdapter(List<Song> songs){
        mList = songs;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SongItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.song_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Song song = mList.get(position);

        holder.binding.title.setText(song.title);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SongItemBinding binding;
        public ViewHolder(SongItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
