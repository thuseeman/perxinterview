package com.perx.perxartist.songs;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.perx.perxartist.R;
import com.perx.perxartist.base.BaseActivity;
import com.perx.perxartist.data.model.Artist;
import com.perx.perxartist.data.remote.RemoteDataSource;
import com.perx.perxartist.databinding.SongsActivityBinding;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class SongsActivity extends BaseActivity {

    private SongsActivityBinding mBinding;
    private SongsVM mSongsVM;
    private CompositeSubscription mSubscription;
    private LinearLayoutManager mLayoutManger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.songs_activity);

        mSubscription = new CompositeSubscription();
        Artist artist = getIntent().getParcelableExtra("artist");
        mSongsVM = new SongsVM(RemoteDataSource.create(), artist);

        mBinding.songs.setHasFixedSize(true);
        mLayoutManger = new LinearLayoutManager(this);
        mBinding.songs.setLayoutManager(mLayoutManger);
        mBinding.songs.setAdapter(new SongsAdapter(mSongsVM.getSongs()));
        mBinding.songs.addOnScrollListener(recyclerViewOnScrollListener);

        mBinding.swipeRefresh.setOnRefreshListener(() -> {
            if (!mSongsVM.isLoading()) {
                mSongsVM.getSongs().clear();
                mSongsVM.setPageNo(1);
                mSongsVM.setLastPage(false);
                loadSongs();
            }
        });

        loadSongs();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSubscription.clear();
    }

    private void loadSongs() {
        mSubscription.add(
                mSongsVM.fetchSongs()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(() -> {
                            mBinding.swipeRefresh.setRefreshing(true);
                            mSongsVM.setLoading(true);
                        })
                        .doOnTerminate(() -> {
                            mBinding.swipeRefresh.setRefreshing(false);
                            mSongsVM.setLoading(false);
                        })
                        .subscribe(response -> {
                            if (response.success) {
                                mBinding.songs.getAdapter().notifyDataSetChanged();
                            } else {
                                if (TextUtils.isEmpty(response.message)) {
                                    showToast(response.message);
                                }
                            }
                        }, t -> {
                            handleServerError(t);
                        })
        );
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = mLayoutManger.getChildCount();
            int totalItemCount = mLayoutManger.getItemCount();
            int firstVisibleItemPosition = mLayoutManger.findFirstVisibleItemPosition();

            if (!mSongsVM.isLoading() && !mSongsVM.isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= SongsVM.PAGE_SIZE) {
                    loadSongs();
                }
            }
        }
    };
}
