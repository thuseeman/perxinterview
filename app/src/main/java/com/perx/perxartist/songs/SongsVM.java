package com.perx.perxartist.songs;

import com.perx.perxartist.data.model.Artist;
import com.perx.perxartist.data.model.Song;
import com.perx.perxartist.data.model.Status;
import com.perx.perxartist.data.remote.ApiService;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Thuseeman on 3/22/2017.
 */

public class SongsVM {
    public static final int PAGE_SIZE = 10;
    private ApiService mApiService;
    private Artist mArtist;
    private List<Song> mSongs = new ArrayList<>();
    private int mPageNo = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    public SongsVM(ApiService apiService, Artist artist) {
        mApiService = apiService;
        mArtist = artist;
    }

    public Artist getArtist() {
        return mArtist;
    }

    public List<Song> getSongs() {
        return mSongs;
    }

    public Observable<Status> fetchSongs() {
        return mApiService.fetchSongs(mArtist.id, mPageNo)
                .map(response -> {
                    Status status = new Status();
                    if (response.isSuccessful()) {
                        if (response.body() != null && response.body().songs != null) {
                            mSongs.addAll(response.body().songs);
                            status.success = true;

                            if (response.body().songs.size() < PAGE_SIZE) {
                                isLastPage = true;
                            } else {
                                mPageNo++;
                            }

                        } else {
                            status.message = "No data available";
                        }
                    } else {
                        status.message = "Please try again";
                    }
                    return status;
                });
    }

    public void setPageNo(int pageNo) {
        mPageNo = pageNo;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public void setLastPage(boolean lastPage) {
        isLastPage = lastPage;
    }

    public int getPageNo() {
        return mPageNo;
    }
}
