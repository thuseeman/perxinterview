package com.perx.perxartist.aritst;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.perx.perxartist.R;
import com.perx.perxartist.data.model.Album;
import com.perx.perxartist.data.model.Artist;
import com.perx.perxartist.databinding.AlbumItemBinding;
import com.perx.perxartist.songs.SongsActivity;

import java.util.List;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.ViewHolder> {
    private List<Album> mList;
    private Artist mArtist;

    public AlbumsAdapter(List<Album> albums, Artist artist){
        mList = albums;
        mArtist = artist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AlbumItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.album_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Album album = mList.get(position);

        holder.binding.title.setText(album.title);

        holder.itemView.setOnClickListener(view -> {
            Context context = view.getContext();
            Intent intent = new Intent(context, SongsActivity.class);
            intent.putExtra("artist", mArtist);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AlbumItemBinding binding;
        public ViewHolder(AlbumItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
