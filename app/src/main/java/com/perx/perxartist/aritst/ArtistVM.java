package com.perx.perxartist.aritst;

import com.perx.perxartist.data.model.Album;
import com.perx.perxartist.data.model.Artist;
import com.perx.perxartist.data.model.Status;
import com.perx.perxartist.data.remote.ApiService;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Thuseeman on 3/22/2017.
 */

public class ArtistVM {
    private List<Album> mAlbums = new ArrayList<>();
    private Artist mArtist;
    private ApiService mApiService;

    public ArtistVM(ApiService apiService, Artist artist) {
        mArtist = artist;
        mApiService = apiService;
    }

    public List<Album> getAlbums() {
        return mAlbums;
    }

    public Artist getArtist() {
        return mArtist;
    }

    public Observable<Status> fetchAlbums(){
        return mApiService.fetchAlbums(mArtist.id)
                .map(response -> {
                    Status status = new Status();
                    if (response.isSuccessful()){
                        if (response.body() != null && response.body().albums != null) {
                            mAlbums.clear();
                            mAlbums.addAll(response.body().albums);
                            status.success = true;
                        }else {
                            status.message = "No data available";
                        }
                    }else {
                        status.message = "Please try again";
                    }
                    return status;
                });
    }
}
