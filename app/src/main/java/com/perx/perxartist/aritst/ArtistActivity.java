package com.perx.perxartist.aritst;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;

import com.perx.perxartist.R;
import com.perx.perxartist.base.BaseActivity;
import com.perx.perxartist.data.model.Artist;
import com.perx.perxartist.data.remote.RemoteDataSource;
import com.perx.perxartist.databinding.ArtistActivityBinding;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Thuseeman on 3/20/2017.
 */

public class ArtistActivity extends BaseActivity {
    public ArtistActivityBinding mBinding;
    private ArtistVM mArtistVM;
    private CompositeSubscription mSubscription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.artist_activity);

        mSubscription = new CompositeSubscription();
        Artist artist = getIntent().getParcelableExtra("artist");
        mArtistVM = new ArtistVM(RemoteDataSource.create(), artist);

        mBinding.albums.setHasFixedSize(true);
        mBinding.albums.setLayoutManager(new LinearLayoutManager(this));
        mBinding.albums.setAdapter(new AlbumsAdapter(mArtistVM.getAlbums(), mArtistVM.getArtist()));

        mBinding.swipeRefresh.setOnRefreshListener(() -> {
            loadAlbums();
        });

        loadAlbums();

        setTitle(mArtistVM.getArtist().name);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSubscription.clear();
    }

    private void loadAlbums(){
        mSubscription.add(
                mArtistVM.fetchAlbums()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(() -> mBinding.swipeRefresh.setRefreshing(true))
                        .doOnTerminate(() -> mBinding.swipeRefresh.setRefreshing(false))
                        .subscribe(response -> {
                            if (response.success) {
                                mBinding.albums.getAdapter().notifyDataSetChanged();
                            }else {
                                if (TextUtils.isEmpty(response.message)) {
                                    showToast(response.message);
                                }
                            }
                        }, t -> {
                            handleServerError(t);
                        })
        );
    }
}
